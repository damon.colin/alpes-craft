Feature: Cards management

  Scenario: Create a card
    Given I create a default card
    When I get the created card
    Then I should have card holder
      | Firstname | Colin                 |
      | Lastname  | Damon                 |
      | Email     | damon.colin@gmail.com |
