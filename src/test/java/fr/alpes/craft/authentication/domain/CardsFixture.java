package fr.alpes.craft.authentication.domain;

import java.util.UUID;

public final class CardsFixture {

  private CardsFixture() {}

  public static ActiveCard activeCard() {
    return new ActiveCard(cardId(), holder());
  }

  public static CardToCreate cardToCreate() {
    return new CardToCreate(holder());
  }

  public static Holder holder() {
    return Holder.builder().firstname("Colin").lastname("Damon").email("damon.colin@gmail.com");
  }

  private static CardId cardId() {
    return new CardId(UUID.fromString("8683e298-5d3d-41a0-bd16-7ce814682eba"));
  }
}
