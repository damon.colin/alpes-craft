package fr.alpes.craft.authentication.domain;

import static fr.alpes.craft.authentication.domain.CardsFixture.*;
import static org.assertj.core.api.Assertions.*;

import fr.alpes.craft.UnitTest;
import org.junit.jupiter.api.Test;

@UnitTest
class ActiveCardTest {

  @Test
  void shouldRevoke() {
    RevokedCard revokedCard = CardsFixture.activeCard().revoke();

    assertThat(revokedCard.holder()).usingRecursiveComparison().isEqualTo(holder());
  }
}
