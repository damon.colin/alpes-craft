package fr.alpes.craft.authentication.domain;

import static org.assertj.core.api.Assertions.*;

import fr.alpes.craft.UnitTest;
import org.junit.jupiter.api.Test;

@UnitTest
class CardIdTest {

  @Test
  void shouldGenerateNewid() {
    assertThat(CardId.newId()).isNotNull().isNotEqualTo(CardId.newId());
  }
}
