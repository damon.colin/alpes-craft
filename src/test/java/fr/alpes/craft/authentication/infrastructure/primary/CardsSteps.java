package fr.alpes.craft.authentication.infrastructure.primary;

import static fr.alpes.craft.cucumber.rest.CucumberRestAssertions.*;

import fr.alpes.craft.cucumber.rest.CucumberRestTemplate;
import fr.alpes.craft.cucumber.rest.CucumberRestTestContext;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;

public class CardsSteps {

  @Autowired
  private CucumberRestTemplate rest;

  @Given("I create a default card")
  public void createDefaultCard() {
    rest.post(
      "/api/cards",
      """
      {
        "holder": {
          "firstname": "Colin",
          "lastname": "Damon",
          "email": "damon.colin@gmail.com"
        }
      }
      """
    );
  }

  @When("I get the created card")
  public void getCreatedCard() {
    rest.get("/api/cards/" + CucumberRestTestContext.getElement("$.id"));
  }

  @Then("I should have card holder")
  public void shouldHaveCardHolder(Map<String, String> holder) {
    assertThatLastResponse().hasOkStatus().hasElement("$.holder").containing(holder);
  }
}
