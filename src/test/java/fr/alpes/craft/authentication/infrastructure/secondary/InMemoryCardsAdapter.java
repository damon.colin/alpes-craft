package fr.alpes.craft.authentication.infrastructure.secondary;

import fr.alpes.craft.authentication.domain.Card;
import fr.alpes.craft.authentication.domain.CardId;
import fr.alpes.craft.authentication.domain.CardsRepository;
import fr.alpes.craft.shared.error.domain.Assert;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.stereotype.Repository;

@Repository
class InMemoryCardsAdapter implements CardsRepository {

  private final Map<CardId, Card> cards = new ConcurrentHashMap<>();

  @Override
  public void save(Card card) {
    Assert.notNull("card", card);

    cards.put(card.id(), card);
  }
}
