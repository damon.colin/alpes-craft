package fr.alpes.craft.authentication.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;

import fr.alpes.craft.JsonHelper;
import fr.alpes.craft.UnitTest;
import fr.alpes.craft.authentication.domain.CardsFixture;
import org.junit.jupiter.api.Test;

@UnitTest
class RestCardToCreateTest {

  @Test
  void shouldConvertToDomain() {
    assertThat(JsonHelper.readFromJson(json(), RestCardToCreate.class).toDomain())
      .usingRecursiveComparison()
      .isEqualTo(CardsFixture.cardToCreate());
  }

  private String json() {
    return """
      {
      "holder": {
        "firstname": "Colin",
        "lastname": "Damon",
        "email": "damon.colin@gmail.com"
      }
    }
    """;
  }
}
