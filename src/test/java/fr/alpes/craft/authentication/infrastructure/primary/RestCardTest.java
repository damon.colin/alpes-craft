package fr.alpes.craft.authentication.infrastructure.primary;

import static fr.alpes.craft.authentication.domain.CardsFixture.*;
import static org.assertj.core.api.Assertions.*;

import fr.alpes.craft.JsonHelper;
import fr.alpes.craft.UnitTest;
import org.junit.jupiter.api.Test;

@UnitTest
class RestCardTest {

  @Test
  void shouldSerializeToJson() {
    assertThat(JsonHelper.writeAsString(RestCard.from(activeCard()))).isEqualTo(json());
  }

  private String json() {
    return """
    {\
    "id":"8683e298-5d3d-41a0-bd16-7ce814682eba",\
    "holder":{\
    "firstname":"Colin",\
    "lastname":"Damon",\
    "email":"damon.colin@gmail.com"\
    }\
    }\
    """;
  }
}
