package fr.alpes.craft.authentication.infrastructure.primary;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import fr.alpes.craft.authentication.domain.Holder;
import fr.alpes.craft.authentication.infrastructure.primary.RestHolder.RestHolderBuilder;

@JsonDeserialize(builder = RestHolderBuilder.class)
class RestHolder {

  private final String firstname;
  private final String lastname;
  private final String email;

  private RestHolder(RestHolderBuilder builder) {
    firstname = builder.firstname;
    lastname = builder.lastname;
    email = builder.email;
  }

  public Holder toDomain() {
    return Holder.builder().firstname(firstname).lastname(lastname).email(email);
  }

  public static RestHolder from(Holder holder) {
    return new RestHolderBuilder()
      .firstname(holder.firstname().value())
      .lastname(holder.lastname().value())
      .email(holder.email().value())
      .build();
  }

  public String getFirstname() {
    return firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public String getEmail() {
    return email;
  }

  @JsonPOJOBuilder(withPrefix = "")
  static class RestHolderBuilder {

    private String firstname;
    private String lastname;
    private String email;

    RestHolderBuilder firstname(String firstname) {
      this.firstname = firstname;

      return this;
    }

    RestHolderBuilder lastname(String lastname) {
      this.lastname = lastname;

      return this;
    }

    RestHolderBuilder email(String email) {
      this.email = email;

      return this;
    }

    RestHolder build() {
      return new RestHolder(this);
    }
  }
}
