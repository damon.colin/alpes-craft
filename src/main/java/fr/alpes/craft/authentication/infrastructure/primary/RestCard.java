package fr.alpes.craft.authentication.infrastructure.primary;

import fr.alpes.craft.authentication.domain.ActiveCard;
import java.util.UUID;

class RestCard {

  private final UUID id;
  private final RestHolder holder;

  private RestCard(UUID id, RestHolder holder) {
    this.id = id;
    this.holder = holder;
  }

  public static RestCard from(ActiveCard card) {
    return new RestCard(card.id().value(), RestHolder.from(card.holder()));
  }

  public UUID getId() {
    return id;
  }

  public RestHolder getHolder() {
    return holder;
  }
}
