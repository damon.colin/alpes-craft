package fr.alpes.craft.authentication.infrastructure.primary;

import fr.alpes.craft.authentication.application.CardsApplicationService;
import fr.alpes.craft.authentication.domain.ActiveCard;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/cards")
class CardsResource {

  private final CardsApplicationService cards;

  public CardsResource(CardsApplicationService cards) {
    this.cards = cards;
  }

  @PostMapping
  ResponseEntity<RestCard> createCard(@RequestBody RestCardToCreate cardToCreate) {
    ActiveCard createdCard = cards.create(cardToCreate.toDomain());

    return new ResponseEntity<RestCard>(RestCard.from(createdCard), HttpStatus.CREATED);
  }
}
