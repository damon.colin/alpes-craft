package fr.alpes.craft.authentication.infrastructure.primary;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.alpes.craft.authentication.domain.CardToCreate;

class RestCardToCreate {

  private final RestHolder holder;

  RestCardToCreate(@JsonProperty("holder") RestHolder holder) {
    this.holder = holder;
  }

  public CardToCreate toDomain() {
    return new CardToCreate(holder.toDomain());
  }
}
