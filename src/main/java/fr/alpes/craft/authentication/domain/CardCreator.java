package fr.alpes.craft.authentication.domain;

import fr.alpes.craft.shared.error.domain.Assert;

public class CardCreator {

  private final CardsRepository cards;

  public CardCreator(CardsRepository cards) {
    this.cards = cards;
  }

  public ActiveCard create(CardToCreate cardToCreate) {
    Assert.notNull("cardToCreate", cardToCreate);

    ActiveCard createdCard = cardToCreate.create();
    cards.save(createdCard);

    return createdCard;
  }
}
