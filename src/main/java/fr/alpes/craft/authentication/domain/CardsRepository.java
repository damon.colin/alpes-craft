package fr.alpes.craft.authentication.domain;

public interface CardsRepository {
  void save(Card card);
}
