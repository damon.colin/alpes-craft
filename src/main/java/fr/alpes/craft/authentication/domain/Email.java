package fr.alpes.craft.authentication.domain;

import fr.alpes.craft.shared.error.domain.Assert;

public record Email(String value) {
  public Email {
    Assert.field("email", value).notBlank().maxLength(255);
  }
}
