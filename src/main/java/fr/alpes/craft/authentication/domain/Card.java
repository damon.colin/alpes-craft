package fr.alpes.craft.authentication.domain;

public sealed interface Card permits ActiveCard, RevokedCard {
  CardId id();
}
