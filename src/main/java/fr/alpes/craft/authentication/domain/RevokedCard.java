package fr.alpes.craft.authentication.domain;

import fr.alpes.craft.shared.error.domain.Assert;

public record RevokedCard(CardId id, Holder holder) implements Card {
  public RevokedCard {
    Assert.notNull("id", id);
    Assert.notNull("holder", holder);
  }
}
