package fr.alpes.craft.authentication.domain;

import fr.alpes.craft.shared.error.domain.Assert;

public record ActiveCard(CardId id, Holder holder) implements Card {
  public ActiveCard {
    Assert.notNull("id", id);
    Assert.notNull("holder", holder);
  }

  public RevokedCard revoke() {
    return new RevokedCard(id(), holder());
  }
}
