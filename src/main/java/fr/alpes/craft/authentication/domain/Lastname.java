package fr.alpes.craft.authentication.domain;

import fr.alpes.craft.shared.error.domain.Assert;

public record Lastname(String value) {
  public Lastname {
    Assert.field("lastname", value).notBlank().maxLength(255);
  }
}
