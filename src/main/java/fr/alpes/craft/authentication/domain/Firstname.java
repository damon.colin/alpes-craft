package fr.alpes.craft.authentication.domain;

import fr.alpes.craft.shared.error.domain.Assert;

public record Firstname(String value) {
  public Firstname {
    Assert.field("fristname", value).notBlank().maxLength(255);
  }
}
