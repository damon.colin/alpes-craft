package fr.alpes.craft.authentication.domain;

import fr.alpes.craft.shared.error.domain.Assert;

public record CardToCreate(Holder holder) {
  public CardToCreate {
    Assert.notNull("holder", holder);
  }

  public ActiveCard create() {
    return new ActiveCard(CardId.newId(), holder);
  }
}
