package fr.alpes.craft.authentication.domain;

import fr.alpes.craft.shared.error.domain.Assert;
import java.util.UUID;

public record CardId(UUID value) {
  public CardId {
    Assert.notNull("cardId", value);
  }

  public static CardId newId() {
    return new CardId(UUID.randomUUID());
  }
}
