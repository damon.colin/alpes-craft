package fr.alpes.craft.authentication.domain;

public class Holder {

  private final Firstname firstname;
  private final Lastname lastname;
  private final Email email;

  private Holder(HolderBuilder builder) {
    firstname = new Firstname(builder.firstname);
    lastname = new Lastname(builder.lastname);
    email = new Email(builder.email);
  }

  public static HolderFirstnameBuilder builder() {
    return new HolderBuilder();
  }

  public Firstname firstname() {
    return firstname;
  }

  public Lastname lastname() {
    return lastname;
  }

  public Email email() {
    return email;
  }

  private static class HolderBuilder implements HolderFirstnameBuilder, HolderLastnameBuilder, HolderEmailBuilder {

    private String firstname;
    private String lastname;
    private String email;

    @Override
    public HolderLastnameBuilder firstname(String firstname) {
      this.firstname = firstname;

      return this;
    }

    @Override
    public HolderEmailBuilder lastname(String lastname) {
      this.lastname = lastname;

      return this;
    }

    @Override
    public Holder email(String email) {
      this.email = email;

      return new Holder(this);
    }
  }

  public interface HolderFirstnameBuilder {
    HolderLastnameBuilder firstname(String firstname);
  }

  public interface HolderLastnameBuilder {
    HolderEmailBuilder lastname(String lastname);
  }

  public interface HolderEmailBuilder {
    Holder email(String email);
  }
}
