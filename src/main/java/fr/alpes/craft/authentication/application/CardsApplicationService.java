package fr.alpes.craft.authentication.application;

import fr.alpes.craft.authentication.domain.ActiveCard;
import fr.alpes.craft.authentication.domain.CardCreator;
import fr.alpes.craft.authentication.domain.CardToCreate;
import fr.alpes.craft.authentication.domain.CardsRepository;
import org.springframework.stereotype.Service;

@Service
public class CardsApplicationService {

  private final CardCreator cardCreator;

  public CardsApplicationService(CardsRepository cards) {
    cardCreator = new CardCreator(cards);
  }

  public ActiveCard create(CardToCreate cardToCreate) {
    return cardCreator.create(cardToCreate);
  }
}
